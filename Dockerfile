FROM docker:1.12

ENV DOCKER_API_VERSION 1.23

VOLUME /var/run/docker.sock

CMD docker events
